const express    = require('express');
const bodyParser = require('body-parser');
const http       = require('http');
const Influx     = require('influx');
const PORT       = 3000;

const influxDB = new Influx.InfluxDB({
    host: 'localhost',
    database: 'sensor_data',
    schema: [
        {
            measurement: 'temp_humid',
            fields: {
                temp: Influx.FieldType.FLOAT,
                humid: Influx.FieldType.FLOAT
            },
            tags: []
        }
    ]
});

const server = express();

// get data from POST method
server.use(bodyParser.text());
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

// enable CORS & cache-control
server.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Cache-Control', 'no-cache');
    next();
});

// endpoint to receive temperature & humidity data.
server.post('/data/temp-humid', function(req, res) {
    let data = req.body;
    let temp = data.temp;
    let humid = data.humid;
    let timestamp  = Date.now(); 

    influxDB
    .writeMeasurement('temp_humid', [{
        timestamp: timestamp,
        fields: {
            temp: temp,
            humid: humid
        }
    }], { precision: 'ms' })
    .then(() => {
        res.status(200).send('OK');
    })
    .catch(err => {
        res.status(500).send(err.stack);
        console.error(`Error writting tag data to sensor_data database! ${err.stack}`);
    });
});

http.createServer(server).listen(PORT, () => {
    console.info(`Collector service running on port ${PORT}`);
});
